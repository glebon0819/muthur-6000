const Tasks = require('../models/Tasks');

// handles a user request for all tasks in the database
exports.all = function(req, res, next) {
	Tasks.findAll({
    	attributes: ['id', 'name', 'description', 'deadline', 'parent_id', 'parent_type', 'precedence', 'position', 'today'],
		order: [
			[ 'position', 'ASC' ]
		]
    }).then(tasks => {
    	res.json(tasks);
    });
};

// handles a user request to create a new task
exports.create = function(req, res, next) {
	console.log(req.body);
	Tasks.findOne({
		attributes: [ 'position' ],
		order: [
			[ 'position', 'DESC' ]
		]
	}).then(task => {
		Tasks.create({
			name: req.body.name,
			description: req.body.description.length > 0 ? req.body.description : null,
			deadline: '2020-09-30',
			position: (typeof task == 'object' && typeof task.position !== 'undefined') ? task.position + 1 : 1,
			precedence: req.body.precedence,
			status: 'todo',
			today: false
		}).then((response) => {
			res.status(200).send();
		});
	});
};

// handles a user request to delete a task
exports.delete = function(req, res, next) {
	Tasks.destroy({
		where: {
			id: req.params.id
		}
	}).then((response) => {
		res.status(200).send();
	});
};

// handles a user request to add/remove a task from today's agenda
exports.today = function(req, res, next) {
	Tasks.update({
		today: req.body.today
	}, {
		where: {
			id: req.params.id
		}
	});
}

exports.update = function(req, res, next) {
	const description = req.body.description;
	Tasks.update({
		name: req.body.name,
		description: typeof description != 'undefined' && description.length > 0 ? req.body.description : null,
		precedence: req.body.precedence
	}, {
		where: {
			id: req.params.id
		}
	}).then((result) => {
		res.status(200).send();
	}).catch((err) => {
		console.error(err);
	});
};

exports.updateOrder = function(req, res, next) {
	/*Tasks.findAll({
    	attributes: ['id', 'position']
    }).then(tasks => {

    });*/
	var updateQueries = [];
	req.body.forEach((task, i) => {
		updateQueries.push(
			Tasks.update({
				position: i
			}, {
				where: {
					id: task.id
				}
			})
		);
	});

	Promise.all(updateQueries).then(responses => {
		res.status(200).send();
	});
};
