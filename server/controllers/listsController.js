const Lists = require('../models/Lists');
const ListItems = require('../models/ListItems');

// add a blank new list item to the database
exports.addItem = function(req, res, next) {
    ListItems.findAll({
    	attributes: [ 'status', 'position' ],
        where: {
            status: true
        }
    }).then(listItems => {
        ListItems.create({
            name: '',
            priority: 2,
            list_id: req.params.id,
            status: true,
            position: listItems.length
        }).then((result) => {
            res.json(JSON.stringify(result));
        });
    });
};

// add a new list to the database
exports.addList = function(req, res, next) {
    Lists.create({
        name: req.body.listName,
        checklist: req.body.checklist
    }).then((result) => {
        res.json(JSON.stringify(result));
    });
};

// send all lists in the database
exports.all = function(req, res, next) {
    Lists.findAll({
    	attributes: [ 'id', 'name', 'description', 'checklist' ]
    }).then(lists => {
    	res.json(lists);
    });
};

// set the status of a list item to false & update the positions of all other items from that list
exports.deleteItem = function(req, res, next) {

    // remove the list item requested
    ListItems.destroy({
        where: {
            id: req.body.id
        }
    }).then(results => {
        ListItems.findAll({
        	attributes: [ 'id', 'position' ],
            where: {
                status: true
            }
        }).then(listItems => {

            // update the existing items to reflect their new order
            listItems.sort((a, b) => {
                return a.position < b.position ? -1 : 1;
            });
            listItems.forEach((listItem, index) => {
                listItem.position = index;
            });

            // update the rows
            var updateQueries = [];
            listItems.forEach((listItem) => {
                updateQueries.push(
                    ListItems.update({
                        position: listItem.position
                    }, {
                        where: {
                            id: listItem.id
                        }
                    })
                );
            });

            // respond when all queries have executed
            Promise.all(updateQueries).then((responses) => {
                res.status(200).send();
            });
        });
    });
};

exports.checkListItem = function(req, res, next) {
    console.log(req.params.id);
    ListItems.update({
        status: req.body.status
    }, {
        where: {
            id: req.params.id
        }
    }).then((result) => {
        res.status(200).send();
    });
}

// send all active list items from a list in the database
exports.listItems = function(req, res, next) {
    ListItems.findAll({
    	attributes: [ 'id', 'name', 'priority', 'position', 'status' ],
        where: {
            list_id: req.params.id
        }
    }).then(listItems => {
    	res.json(listItems);
    });
};

// update list items to reflect changes made by the user
exports.saveList = function(req, res, next) {
    console.log(req.body);
    var updateQueries = [];
    var updatedListItems = {};

    for(var listItemID in req.body.updatedItemNames) {
        if(!isNaN(listItemID))
            updatedListItems[listItemID] = req.body.updatedItemNames[listItemID];
    }

    for(var listItemID in updatedListItems) {
        updateQueries.push(
            ListItems.update({
                name: req.body.updatedItemNames[listItemID]
            }, {
                where: {
                    id: listItemID
                }
            })
        );
    }
    Promise.all(updateQueries).then((responses) => {
        //console.log(responses);
        res.status(200).send();
    });
}

// update the order of the list items in a list
exports.updateOrder = function(req, res, next) {
    var updateQueries = [];
    req.body.forEach((listItem) => {
        updateQueries.push(
            ListItems.update({
                position: listItem.position
            }, {
                where: {
                    id: listItem.id
                }
            })
        );
    });
    Promise.all(updateQueries).then((responses) => {
        res.status(200).send();
    }).catch(error => {
        console.error(error);
        res.status(500).send();
    });;
}
