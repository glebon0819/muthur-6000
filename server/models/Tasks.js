const Sequelize = require('sequelize');
console.log(process.env.DATABASE_URL);
const sequelize = new Sequelize(process.env.DATABASE_URL);

// serialize model of the applications table
const Model = Sequelize.Model;
class Tasks extends Model {};
Tasks.init({
	name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	description: {
		type: Sequelize.STRING
	},
	deadline: {
		type: Sequelize.DATE,
		allowNull: false
	},
	parent_id: {
		type: Sequelize.INTEGER
	},
	parent_type: {
		type: Sequelize.STRING
	},
	precedence: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	status: {
		type: Sequelize.STRING,
		allowNull: false
	},
	position: {
		type: Sequelize.INTEGER,
		allowNull: false
	},
	today: {
		type: Sequelize.BOOLEAN,
		allowNull: false
	}
}, {
    createdAt: 'created_at',
	updatedAt: 'updated_at',
	sequelize,
	modelName: 'tasks'
});
module.exports = Tasks;
