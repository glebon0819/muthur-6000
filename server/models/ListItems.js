const Sequelize = require('sequelize');
console.log(process.env.DATABASE_URL);
const sequelize = new Sequelize(process.env.DATABASE_URL);

// serialize model of the list_items table
const Model = Sequelize.Model;
class ListItems extends Model {};
ListItems.init({
    name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	description: {
		type: Sequelize.STRING
	},
    priority: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    list_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    status: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
    position: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    sequelize,
    modelName: 'list_items'
});
module.exports = ListItems;
