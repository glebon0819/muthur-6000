const Sequelize = require('sequelize');
console.log(process.env.DATABASE_URL);
const sequelize = new Sequelize(process.env.DATABASE_URL);

// serialize model of the applications table
const Model = Sequelize.Model;
class Lists extends Model {};
Lists.init({
    name: {
		type: Sequelize.STRING,
		allowNull: false
	},
	description: {
		type: Sequelize.STRING
	},
    checklist: {
        type: Sequelize.BOOLEAN
    }
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    sequelize,
    modelName: 'lists'
});
module.exports = Lists;
