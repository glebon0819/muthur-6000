const express = require('express');
const path = require('path');

// imports environment variables if working on a local version of the code
var error = require('dotenv').config({
    path: './.env'
});

// imports the application's controllers
const tasksController = require('./controllers/tasksController');
const listsController = require('./controllers/listsController');

// creates the Express instance
const app = express();
const port = process.env.PORT || 3000;

// allows the application to automatically parse request bodies as JSON
app.use(express.json());

// defines the home page of the application, as generated by Webpack
const DIST_DIR = path.join(__dirname, '../dist');
const HTML_FILE = path.join(DIST_DIR, 'index.html');
app.use(express.static(DIST_DIR));

// routes all task-related API requests to the taks controller
app.get('/api/tasks/all', tasksController.all);
app.post('/api/tasks/create', tasksController.create);
app.post('/api/tasks/reorder', tasksController.updateOrder);
app.post('/api/tasks/:id', tasksController.update);
app.delete('/api/tasks/:id', tasksController.delete);
app.post('/api/tasks/today/:id', tasksController.today);

// routes all list-related API requests to the lists controller
app.get('/api/lists/all', listsController.all);
app.get('/api/lists/:id', listsController.listItems);
app.post('/api/lists/addList', listsController.addList);
app.post('/api/lists/:id/addItem', listsController.addItem);
app.post('/api/lists/:id/deleteItem', listsController.deleteItem);
app.post('/api/lists/:id/check', listsController.checkListItem);
app.post('/api/lists/:id/save', listsController.saveList);
app.post('/api/lists/:id/updateOrder', listsController.updateOrder);

// sends the user the home page if they specify no path
app.get('/', (req, res) => {
    res.sendFile(HTML_FILE);
});

// sends the user a 404 error if they request anything else
app.get('/*', (req, res) => {
    res.status(404).send();
});

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Bruh moment');
});

// starts up the server
app.listen(port, () => console.log('App listening on port: ' + port));
