import React from 'react';

export default class Window extends React.Component {
    componentDidUpdate(prevProps) {}

    render() {
        console.log(this.props.style);
        return (
            <div className='window' style={ this.props.style }>
                <div className='window-banner'>{ this.props.title }</div>
                <div className='window-banner-content'>{ this.props.children }</div>
            </div>
        );
    }
}
