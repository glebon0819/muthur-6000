import React from 'react';
import Modal from 'react-bootstrap/Modal';
import { ReactSVG } from 'react-svg';
import Toggle from 'react-bootstrap-toggle';

import Button from './Button';

import Trashcan from '../trashcan.svg';

export default class TaskCreator extends React.Component {
    componentDidUpdate(prevProps) {
        if(JSON.stringify(this.props) !== JSON.stringify(prevProps)) {
            var task = this.props.task;
            this.setState({
                deleting: false,
                show: this.props.showPopup,
                descriptionValue: (task !== null && typeof task !== 'undefined' && task.description !== null) ? task.description : '',
                objectiveValue: (task !== null && typeof task !== 'undefined') ? task.name : '',
                precedenceValue: (typeof task == 'undefined' || task == null) ? 4 : task.precedence,
                toggleActive: false
            });
        }
    }
    constructor(props){
        super(props);
        var task = this.props.task;
        this.state = {
            deleting: false,
            descriptionValue: '',
            objectiveValue: '',
            precedenceValue: (typeof task == 'undefined' || task == null) ? 4 : task.precedence,
            show: this.props.showPopup,
            toggleActive: false
        };
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleObjectiveChange = this.handleObjectiveChange.bind(this);
        this.handlePrecedenceChange = this.handlePrecedenceChange.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.onToggle = this.onToggle.bind(this);
        this.submit = this.submit.bind(this);
    }
    handleDescriptionChange(event) {
        this.setState({
            descriptionValue: event.target.value
        });
    }
    handleObjectiveChange(event) {
        this.setState({
            objectiveValue: event.target.value
        });
    }
    handlePrecedenceChange(event) {
        this.setState({
            precedenceValue: event.target.value
        });
    }
    hideModal() {
        this.setState({
            deleting: false,
            show: false
        });
    }
    onToggle() {
        this.setState({
            deleting: !this.state.deleting,
            toggleActive: !this.state.toggleActive
        });
    }
    render() {
        var precedence;
        if(this.props.task === null || typeof this.props.task == 'undefined')
            precedence = null;
        else
            precedence = this.props.task.precedence;
        return (
            <Modal
                animation={ false }
                backdrop="static"
                centered
                dialogClassName="modal-60w"
                keyboard={ false }
                onHide={ () => this.props.callback(false) }
                show={ this.state.show }>
                <Modal.Header closeButton>
                    <Modal.Title>Task Editor</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='row'>
                        <div className='col-sm-7'>
                            <h5>Summary:</h5>
                            <input
                                disabled={ this.state.deleting }
                                onChange={ this.handleObjectiveChange }
                                style={{ fontSize: '12px', width: '100%' }}
                                type='text'
                                value={ this.state.objectiveValue } />
                            <br /><br />
                            <h5>Description:</h5>
                            <textarea
                                disabled={ this.state.deleting }
                                onChange={ this.handleDescriptionChange }
                                rows={ 3 }
                                style={{ fontSize: '10px', width: '100%' }}
                                value={ this.state.descriptionValue }></textarea>
                        </div>
                        <div className='col-sm-5'>
                            <h5>Status</h5>
                            <select
                                style={{
                                    fontSize: '12px',
                                    padding: '8px',
                                    width: '100%'
                                }}>
                                <option>To-Do</option>
                                <option>In Progress</option>
                                <option>Completed</option>
                            </select>
                            <br /><br />
                            <h5>Priority Level:</h5>
                            <select
                                disabled={ this.state.deleting }
                                onChange={ this.handlePrecedenceChange }
                                style={{
                                    fontSize: '12px',
                                    padding: '8px',
                                    width: '100%'
                                }}
                                value={ this.state.precedenceValue }>
                                <option value={ 1 }>One (Immediately)</option>
                                <option value={ 2 }>Two (Urgent)</option>
                                <option value={ 3 }>Three (Sometime Soon)</option>
                                <option value={ 4 }>Four (Whenever You Can)</option>
                            </select>
                            <br /><br />
                            <h5>Due Date:</h5>
                            <input
                                disabled={ this.state.deleting }
                                style={{ width: '100%' }} type='text' />
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className='row' style={{ width: '100%' }}>
                        <div className='col-sm-6'>
                            <div className='row'>
                                <div className='col-sm-1'>
                                    <ReactSVG
                                        src={ Trashcan }
                                        beforeInjection={(svg) => {
                                            svg.classList.add('svg-poop')
                                            svg.setAttribute('style', 'fill: #000')
                                        }}
                                        fallback={() => <span>loading</span>} />
                                </div>
                                <div className='col-sm-2'>
                                    <Toggle
                                        active={ this.state.toggleActive }
                                        handleClassName='white'
                                        off={ <span></span> }
                                        offstyle='light'
                                        on={ <span></span> }
                                        onClick={ this.onToggle }
                                        onstyle='danger'
                                        size='xs'
                                        style={{ border: '1px solid darkgrey', borderRadius: 0, height: '25px', width: '50px' }} />
                                </div>
                                <div className='col-sm-9'></div>
                            </div>
                        </div>
                        <div className='col-sm-3'>
                            <Button
                                onClick={ () => this.submit() }
                                text='Save' />
                        </div>
                        <div className='col-sm-3'>
                            <Button
                                onClick={ () => this.props.callback(false) }
                                text='Cancel' />
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }
    submit() {
        //this.hideModal();
        this.props.callback({
            delete: this.state.deleting,
            id: typeof this.props.task !== 'undefined' && this.props.task !== null ? this.props.task.id : null,
            name: this.state.objectiveValue,
            description: this.state.descriptionValue,
            precedence: this.state.precedenceValue
        });
    }
}
