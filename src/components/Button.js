import React from 'react';
import { ReactSVG } from 'react-svg';

export default class Button extends React.Component {
    render() {
        var content;
        if(this.props.icon != undefined && this.props.icon != null) {
            content = (
                <ReactSVG
                    src={ this.props.icon }
                    beforeInjection={(svg) => {
                        svg.classList.add('svg-class-name')
                        svg.setAttribute('style', 'fill: #FFF')
                    }}
                    fallback={() => <span>icon</span>} />
            );
        } else {
            content = this.props.text;
        }
        return <div className={ (this.props.icon != undefined && this.props.icon != null) ? 'button icon' : 'button' } onClick={ this.props.onClick }>{ content }</div>;
    }
}
