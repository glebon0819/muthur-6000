import React from 'react';
import { DragDropContext, Droppable, Draggable, Provided } from 'react-beautiful-dnd';
import { ReactSVG } from 'react-svg';

import Button from './Button';
import Task from './Task';
import TaskCreator from './TaskCreator';

import loading from '../loading.gif';
import dragHandle from '../drag_handle.svg';

const reorder = (list, startIndex, endIndex) => {
    var newList = Array.from(list);
    const [removed] = newList.splice(startIndex, 1);
    newList.splice(endIndex, 0, removed);
    var result = [];
    newList.forEach((listItem, i) => {
        listItem.position = i;
        result.push(listItem);
    });
    return result;
};

export default class Tasks extends React.Component {
    componentDidMount() {
        fetch('/api/tasks/all')
        .then(res => res.json())
        .then((res) => {
            this.setState({
                tasks: res
            });
        }, (err) => {
            console.error(err);
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.update) {
            fetch('/api/tasks/all')
            .then(res => res.json())
            .then((res) => {
                this.setState({
                    tasks: sortTasks(res),
                    update: false
                });
            }, (err) => {
                console.error(err);
            });
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            showTaskEditor: false,
            tasks: [],
            update: false
        };
        this.handleTaskEditorClose = this.handleTaskEditorClose.bind(this);
        this.hideTaskEditor = this.hideTaskEditor.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    handleTaskEditorClose(result) {
        if(result) {
            if(result.id === null) {
                fetch('/api/tasks/create', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(result)
                }).then((res) => {
                    this.hideTaskEditor(() => {
                        this.setState({
                            update: true
                        });
                    });
                });
            } else if(result.delete) {
                fetch('/api/tasks/' + result.id, {
                    method: 'DELETE'
                }).then((res) => {
                    this.hideTaskEditor(() => {
                        this.setState({
                            update: true
                        });
                    });
                });
            } else {
                //updateTask(result);
                fetch('/api/tasks/' + result.id, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(result)
                }).then((res) => {
                    this.hideTaskEditor(() => {
                        this.setState({
                            update: true
                        });
                    });
                });
            }
        } else {
            this.hideTaskEditor();
        }
    }

    hideTaskEditor(callback) {
        this.setState({
            showTaskEditor: false,
            task: false
        }, callback);
    }

    onDragEnd(result) {
        console.log('drag ended');
        const tasks = reorder(this.state.tasks, result.source.index, result.destination.index);

        this.setState({
            tasks
        }, () => {
            fetch('/api/tasks/reorder', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(tasks)
            });
        });
    }

    onDragStart() {
        console.log('drag started');
    }

    render() {
        var tasks;
        if(this.state.tasks.length > 0) {
            tasks = this.state.tasks.map((task, i) => {
                return (
                    <Task
                        key={ 'task-' + i }
                        task={ task }
                        sendData={ () => {
                            this.setState({
                                editing: task,
                                showTaskEditor: true
                            });
                        }} />
                );
            });
        }
        return (
            <DragDropContext onDragEnd={ this.onDragEnd } onDragStart={ this.onDragStart }>
                <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                        <div
                            {...provided.droppableProps}
                            id='tasks'
                            ref={provided.innerRef}
                            style={{
                                height: this.props.appHeight - 132,
                                maxHeight: this.props.appHeight - 132,
                                overflowX: 'hidden',
                                overflowY: 'scroll'
                            }}>
                            { this.state.tasks.map((task, i) => (
                                <Draggable key={task.id} draggableId={ 'draggable' + task.id } index={i}>
                                    {(provided, snapshot) => (
                                        <div ref={provided.innerRef} {...provided.draggableProps}>
                                            <div className='row'>
                                                <div {...provided.dragHandleProps} className='col-sm-1' style={{ alignItems: 'center', display: 'flex', justifyContent: 'center', paddingRight: 0 }}>
                                                    <ReactSVG
                                                        src={ dragHandle }
                                                        beforeInjection={(svg) => {
                                                            svg.classList.add('svg-class-name')
                                                            svg.setAttribute('style', 'fill: #FFF')
                                                        }}
                                                        fallback={() => <span>drag</span>} />
                                                </div>
                                                <div className='col-sm-11' style={{ paddingLeft: 0 }}>
                                                    <Task
                                                        key={ 'task-' + i }
                                                        task={ task }
                                                        sendData={ () => {
                                                            this.setState({
                                                                editing: task,
                                                                showTaskEditor: true
                                                            });
                                                        }} />
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            { provided.placeholder }
                            <Button
                                onClick={ () => {
                                    this.setState({
                                        editing: null,
                                        showTaskEditor: true
                                    });
                                }}
                                text='Create New Task' />
                            <TaskCreator
                                callback={ this.handleTaskEditorClose }
                                showPopup={ this.state.showTaskEditor }
                                task={ this.state.editing } />
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
        );
    }

    updateTask(task) {
        /*fetch('/api/tasks/' + task.id, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(task)
        }).then((res) => console.log(res));*/
    }
}
