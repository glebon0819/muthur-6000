import React from 'react';
import Modal from 'react-bootstrap/Modal';

import Button from './Button';

/*
<div className='window'>
    <div className='window-banner'>Popup</div>
    <div className='window-content'>
        <p>Text text text</p>
    </div>
</div>
*/

export default class Popup extends React.Component {
    componentDidUpdate(prevProps) {
        if(JSON.stringify(this.props) !== JSON.stringify(prevProps)) {
            this.setState({
                show: this.props.showPopup
            });
        }
    }
    constructor(props){
        super(props);
        this.state = {
            show: this.props.showPopup,
            value: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.submit = this.submit.bind(this);
    }
    handleChange(event) {
        this.setState({
            value: event.target.value
        });
    }
    hideModal() {
        this.setState({
            show: false
        });
    }
    render() {
        return (
            <Modal
                backdrop="static"
                centered
                keyboard={ false }
                onHide={ () => this.props.callback(false) }
                show={ this.state.show }>
                <Modal.Dialog>
                    <Modal.Header closeButton>
                        <p>Please enter your new list name:</p>
                    </Modal.Header>
                    <Modal.Body>
                        <input
                            onChange={ this.handleChange }
                            type='text' />
                        <div className='row'>
                            <div className='col-sm-6'>
                                <Button
                                    onClick={ () => this.submit() }
                                    text='Submit' />
                            </div>
                            <div className='col-sm-6'>
                                <Button
                                    onClick={ () => this.props.callback(false) }
                                    text='Cancel' />
                            </div>
                        </div>
                    </Modal.Body>
                </Modal.Dialog>
            </Modal>
        );
    }
    submit() {
        //this.hideModal();
        this.props.callback(this.state.value);
    }
}
