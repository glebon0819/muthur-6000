import React from 'react';
import Draggable from 'react-draggable';

export default class Task extends React.Component {
    constructor(props) {
        super(props);
        this.handleStart = this.handleStart.bind(this);
        this.handleStop = this.handleStop.bind(this);
    }
    handleDrag() {
        console.log(this.state.position);
    }
    handleStart() {
        this.props.updateDragging(true);
    }
    handleStop() {
        this.props.updateDragging(false);
    }
    render() {
        /*return (
            <Draggable
                handle='.task-handle'
                onStart={ this.handleStart }
                onStop={ this.handleStop }>
                <div className='task'>
                    <div className='task-handle'>Handle</div>
                    <div className='task-body' onClick={ () => this.props.sendData(this.props.task) }>
                        { this.props.task.name }
                    </div>
                </div>
            </Draggable>
        );*/
        var precedence = this.props.task.precedence;
        return (
            <div className={ 'task' + (precedence == 1 ? ' emergency' : precedence == 2 ? ' urgent' : precedence == 3 ? ' soon' : '') } onClick={ () => this.props.sendData(this.props.task) }>
                <div className='task-body'>
                    { this.props.task.name }
                </div>
            </div>
        );
    }
}
