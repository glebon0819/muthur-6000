import React from 'react';
import ReactHeight from 'react-height';

import TaskCreator from './TaskCreator';
import Task from './Task';
import Button from './Button';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

// fake data generator
const getItems = (count, offset = 0) =>
    Array.from({ length: count }, (v, k) => k).map(k => ({
        id: `item-${k + offset}`,
        name: `item ${k + offset}`
    }));

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: grid
});

export default class App extends React.Component {

    callback(input) {
        if(input !== false) {
            var task = input;
            fetch('/api/tasks/create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(task)
            }).then((res) => {
                this.setState({
                    editing: null,
                    showTaskCreator: false
                });
            });
        } else {
            this.setState({
                showTaskCreator: false
            });
        }
    }

    // retrieves all tasks from the database
    componentDidMount() {
        fetch('/api/tasks/all')
        .then(res => res.json())
        .then((result) => {
            let todoTasks = [],
                todaysTasks = [];

            // puts each task that is supposed to be done today in an array
            // puts all other tasks in another array
            result.forEach((task) => task.today ? todaysTasks.push(task) : todoTasks.push(task));

            // sets the state according to these arrays
            this.setState({
                tasks: todoTasks,
                selected: todaysTasks
            });
        }, (error) => {
            console.error(error);
        });
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('updating');
        if(this.state.editing === null && prevState.editing !== null) {
            console.log('refreshing');
            fetch('/api/tasks/all')
            .then(res => res.json())
            .then((result) => {
                let todoTasks = [],
                    todaysTasks = [];

                // puts each task that is supposed to be done today in an array
                // puts all other tasks in another array
                result.forEach((task) => task.today ? todaysTasks.push(task) : todoTasks.push(task));

                // sets the state according to these arrays
                this.setState({
                    tasks: todoTasks,
                    selected: todaysTasks
                });
            }, (error) => {
                console.error(error);
            });
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            editing: null,
            items: getItems(10),
            //selected: getItems(5, 10),
            selected: [],
            //selected2: getItems(10, 15),
            selected2: [],
            showTaskCreator: false,
            showTaskEditor: false,
            tasks: []
        };

        /**
         * A semi-generic way to handle multiple lists. Matches
         * the IDs of the droppable container to the names of the
         * source arrays stored in the state.
         */
        this.id2List = {
            droppable: 'tasks',
            droppable2: 'selected',
            droppable3: 'selected2'
        };
        this.callback = this.callback.bind(this);
        this.getList = this.getList.bind(this);
        this.handleTaskEdit = this.handleTaskEdit.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    getList(id) {
        return this.state[this.id2List[id]];
    }

    handleTaskEdit(result) {
        console.log(result);
        if(!result) {
            this.setState({
                editing: null,
                showTaskEditor: false
            });
        } else {
            if(result.delete) {
                fetch('/api/tasks/' + result.id, {
                    method: 'DELETE'
                }).then((res) => {
                    console.log(res);
                    this.setState({
                        editing: null,
                        showTaskEditor: false
                    });
                }, (err) => {
                    console.error(err);
                });
            } else {
                console.log('updato');
            }
        }
    }

    onDragEnd(result) {
        const { source, destination } = result;

        // dropped outside the list
        if(!destination) {
            return;
        }

        if(source.droppableId === destination.droppableId) {
            const tasks = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            let state = { tasks };

            if (source.droppableId === 'droppable2') {
                state = { selected: tasks };
            } else if (source.droppableId === 'droppable3') {
                state = { selected2: tasks };
            }

            this.setState(state);
        } else {
            const newIndex = destination.index,
            result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );

            // updates the content for the droppables that were altered
            for(var droppableId in result) {
                this.setState({
                    [this.id2List[droppableId]]: result[droppableId]
                });
            }

            // requests that the task's state get updated in the database
            let draggedTask = result[destination.droppableId][newIndex],
            destinationId = destination.droppableId;
            fetch('/api/tasks/today/' + draggedTask.id, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    today: destinationId == 'droppable2' ? true : false
                })
            }).then(res => {
                console.log(res);
            });
        }
    };

    // Normally you would want to split things out into separate components.
    // But in this example everything is just done in one place for simplicity
    render() {
        //console.log(this.state.items);
        return (
            <div className='row' style={{ margin: 0 }}>
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <Droppable droppableId="droppable3">
                        {(provided, snapshot) => (
                            <div
                                className='col-sm'
                                ref={provided.innerRef}
                                /*style={{ backgroundColor: 'red' }}*/>
                                <h5>Today's Schedule</h5>
                                {
                                    this.state.selected2.map((item, index) => {
                                        return (
                                            <Draggable
                                                key={item.id}
                                                draggableId={'draggable' + item.id}
                                                index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        /*style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}*/>
                                                        <Task sendData={ () => this.setState({ showTaskEditor: true, editing: item }) } task={ item } />
                                                    </div>
                                                )}
                                            </Draggable>
                                        );
                                    })
                                }
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                    <Droppable droppableId="droppable2">
                        {(provided, snapshot) => (
                            <div
                                className='col-sm'
                                ref={provided.innerRef}>
                                <h5>Today's Agenda</h5>
                                {this.state.selected.map((item, index) => (
                                    <Draggable
                                        key={item.id}
                                        draggableId={'draggable' + item.id}
                                        index={index}>
                                        {(provided, snapshot) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}>
                                                <Task sendData={ () => this.setState({ showTaskEditor: true, editing: item }) } task={ item } />
                                            </div>
                                        )}
                                    </Draggable>
                                ))}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                    <Droppable droppableId="droppable">
                        {(provided, snapshot) => (
                            <div
                                className='col-sm'
                                ref={provided.innerRef}
                                /*style={{ height: this.props.appHeight - 16 - 4, maxHeight: this.props.appHeight - 16 - 4, overflowX: 'none', overflowY: 'scroll' }}*/>
                                <h5>Your Tasks</h5>
                                <div style={{ height: this.props.appHeight - 16 - 4 - 24 - 8 - 44, maxHeight: this.props.appHeight - 16 - 4 - 24 - 8 - 56, overflowX: 'none', overflowY: 'scroll' }}>
                                    {this.state.tasks.map((item, index) => (
                                        <Draggable
                                            key={item.id}
                                            draggableId={'draggable' + item.id}
                                            index={index}>
                                            {(provided, snapshot) => (
                                                <div
                                                    ref={provided.innerRef}
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                    /*style={getItemStyle(
                                                        snapshot.isDragging,
                                                        provided.draggableProps.style
                                                    )}*/>
                                                    <Task sendData={ () => this.setState({ showTaskEditor: true, editing: item }) } task={ item } />
                                                </div>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </div>
                                <Button text='Create New Task' onClick={ () => this.setState({ showTaskCreator: true, editing: true }) } />
                                <TaskCreator
                                    callback={ this.callback }
                                    showPopup={ this.state.showTaskCreator } />
                                <TaskCreator
                                    callback={ this.handleTaskEdit }
                                    showPopup={ this.state.showTaskEditor }
                                    task={ this.state.editing } />
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </div>
        );
    }
}
