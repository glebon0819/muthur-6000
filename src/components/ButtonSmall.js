import React from 'react';

export default class ButtonSmall extends React.Component {
    render() {
        return (
            <div className='button-small' onClick={ this.props.onClick }>{ this.props.text }</div>
        );
    }
}
