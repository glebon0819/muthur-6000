import React from 'react';

import ButtonSmall from '../ButtonSmall';

export default class ListItem extends React.Component {
    componentDidUpdate(prevProps) {
        if(JSON.stringify(prevProps) !== JSON.stringify(this.props)) {
            this.setState({
                value: this.props.value
            });
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            checked: this.props.item.status,
            value: this.props.value
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
    }
    handleChange(event) {
        this.setState({
            value: event.target.value
        });
        var newValues = {};
        if(this.props.id !== null && this.props.id !== undefined) {
            newValues[this.props.id] = event.target.value;
            this.props.updateListItemName(newValues);
        } /*else {
            newValues[this.props.index] = event.target.value;
            this.props.addNewListItemName(newValues);
        }*/
    }
    handleCheck() {
        var id = this.props.id;
        this.setState({
            checked: !this.state.checked
        }, () => {
            this.props.handleChecklistItemCheck(id, this.state.checked);
        });
    }
    render() {
        if(this.props.checklistItem) {
            var inputStyle = {};
            if(this.state.checked)
                inputStyle = { textDecoration: 'line-through' };
            return (
                <div style={{ paddingLeft: 4, paddingTop: 4, paddingRight: 4 }}>
                    <div className='row'>
                        <div className='col-sm-1' style={{ paddingRight: 0 }}>
                            <input
                                checked={ this.state.checked }
                                onChange={ this.handleCheck }
                                type='checkbox' />
                        </div>
                        <div className='col-sm-10' style={{ paddingLeft: 0, paddingRight: 0 }}>
                            <input
                                className='list-item'
                                disabled={ this.state.checked }
                                style={ inputStyle }
                                type='text'
                                onChange={ this.handleChange }
                                value={ this.state.value } />
                        </div>
                        <div className='col-sm-1' style={{ paddingLeft: 0 }}>
                            <ButtonSmall
                                onClick={ () => this.props.deleteItem(this.props.id) }
                                text='X' />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div style={{ paddingLeft: 4, paddingTop: 4, paddingRight: 4 }}>
                    <div className='row'>
                        <div className='col-sm-11' style={{ paddingRight: 0 }}>
                            <input
                                className='list-item'
                                type='text'
                                onChange={ this.handleChange }
                                value={ this.state.value } />
                        </div>
                        <div className='col-sm-1' style={{ paddingLeft: 0 }}>
                            <ButtonSmall
                                onClick={ () => this.props.deleteItem(this.props.id) }
                                text='X' />
                        </div>
                    </div>
                </div>
            );
        }
    }
}
