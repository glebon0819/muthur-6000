import React from 'react';
import { ReactSVG } from 'react-svg';

import dragHandle from '../../drag_handle.svg';
import ListItem from './ListItem';
import { DragDropContext, Droppable, Draggable, Provided } from 'react-beautiful-dnd';

const getItems = count =>
    Array.from({ length: count }, (v, k) => k).map(k => ({
        id: `item-${k}`,
        content: `item ${k}`
    }));


const reorder = (list, startIndex, endIndex) => {
    var newList = Array.from(list);
    const [removed] = newList.splice(startIndex, 1);
    newList.splice(endIndex, 0, removed);
    var result = [];
    newList.forEach((listItem, i) => {
        listItem.position = i;
        result.push(listItem);
    });
    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : '#000',

    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "lightblue" : "lightgrey",
    padding: grid,
    width: 250
});

export default class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checklist: this.props.list.checklist,
            deletedItems: [],
            listItems: null,
            listID: this.props.list.id,
            updatingOrder: false
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
        this.onDragStart = this.onDragStart.bind(this);
    }

    // retrieve all list items from the database when component mounts
    componentDidMount() {
        //var list = this.props.list.map();
        fetch('/api/lists/' + this.props.list.id)
        .then(res => res.json())
        .then((result) => {
            this.setState({
                listItems: result.sort(this.compareListItems)
            });
        }, (error) => {
            console.error(error);
        });
    }
    componentDidUpdate(prevProps) {
        this.setState({
            checklist: this.props.list.checklist
        });
        //var list = this.props.list.map();
        fetch('/api/lists/' + this.props.list.id)
        .then(res => res.json())
        .then((result) => {
            this.setState({
                listItems: result.sort(this.compareListItems)
            });
        }, (error) => {
            console.error(error);
        });
    }

    // remove the item from the screen and add it to the list of items for permanent deletion
    deleteItem(itemID) {
        /*if(itemID !== null) {

            // remove the item from the local list and add its ID to the list of deleted IDs
            this.setState(state => {
                const deletedItems = state.deletedItems.concat([ itemID ]);
                return {
                    deletedItems: deletedItems,
                    listItems: state.listItems.filter(listItem => listItem.id != itemID),
                    listID: state.listID
                };
            });

            // add the deleted item's ID to the master list so it can be saved permanently
            this.props.deleteNewListItemName(itemID);
        } else {
            this.props.deleteNewListItem();
        }*/
        this.props.toggleLoading();
        this.setState(state => {
            return {
                deletedItems: state.deletedItems,
                listItems: state.listItems.filter(listItem => listItem.id != itemID),
                listID: state.listID
            };
        });
        var itemNames = { id: itemID };
        fetch('/api/lists/' + this.state.listID + '/deleteItem', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(itemNames)
        }).then((res) => {
            this.props.toggleLoading();
        });
    }
    onDragEnd(result) {
        //this.props.toggleLoading();

        // dropped outside the list
        if (!result.destination) {
            return;
        }
        const listItems = reorder(this.state.listItems, result.source.index, result.destination.index);

        this.setState({
            listItems,
            updatingOrder: true
        });

        fetch('/api/lists/' + this.state.selectedListID + '/updateOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(listItems)
        }).then((res) => {
            this.props.toggleLoading();
        });
    }
    onDragStart() {
        this.props.toggleLoading();
    }
    render() {
        var content = [];
        if(this.state.listItems === null)
            content = <h6>Loading { this.props.list.name } content. Please wait...</h6>;
        else if(this.state.listItems.length === 0)
            content = <h6 style={{ height: this.props.appHeight - 140 }}>There are no items to display</h6>;
        else {
            // filter out any items awaiting deletion before render
            /*var filteredListItems = this.state.listItems.filter(listItem => {
                return !this.state.deletedItems.includes(listItem.id);
            });
            content = filteredListItems.map((listItem, idx) => {
                return (
                    <ListItem
                        id={ listItem.id }
                        index={ this.state.listItems.length + i }
                        key={ 'listItem-' + idx }
                        value={ listItem.name }
                        addNewListItemName={ this.props.addNewListItemName }
                        deleteItem={ this.deleteItem }
                        updateListItemName={ this.props.updateListItemName } />
                );
            });*/
            content = (<DragDropContext onDragEnd={ this.onDragEnd } onDragStart={ this.onDragStart }>
                <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                        <div
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            style={{
                                height: this.props.appHeight - 132,
                                maxHeight: this.props.appHeight - 132,
                                overflowX: 'hidden',
                                overflowY: 'scroll'
                            }}>
                            {this.state.listItems.map((item, index) => (
                                <Draggable isDragDisabled={ this.props.loading } key={item.id} draggableId={ 'draggable' + item.id } index={index}>
                                    {(provided, snapshot) => (
                                        <div ref={provided.innerRef} {...provided.draggableProps} style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}>
                                            <div className='row'>
                                                <div {...provided.dragHandleProps} className='col-sm-1' style={{ alignItems: 'center', display: 'flex', justifyContent: 'center', paddingRight: 0 }}>
                                                    <ReactSVG
                                                        src={ dragHandle }
                                                        beforeInjection={(svg) => {
                                                            svg.classList.add('svg-class-name')
                                                            svg.setAttribute('style', 'fill: #FFF')
                                                        }}
                                                        fallback={() => <span>drag</span>} />
                                                </div>
                                                <div className='col-sm-11' style={{ paddingLeft: 0 }}>
                                                    <ListItem
                                                        checklistItem={ this.state.checklist }
                                                        handleChecklistItemCheck={ this.props.handleChecklistItemCheck }
                                                        id={ item.id }
                                                        index={ index }
                                                        item={ item }
                                                        key={ 'listItem-' + index }
                                                        value={ item.name }
                                                        /*addNewListItemName={ this.props.addNewListItemName }*/
                                                        deleteItem={ this.deleteItem }
                                                        updateListItemName={ this.props.updateListItemName } />
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </Draggable>
                            ))}

                            { provided.placeholder }
                        </div>
                    )}
                </Droppable>
            </DragDropContext>);
        }
        return (
            <div id='list'>{ content }</div>
        );
    }

    // make the component update only if changes were made to prevent recursion
    shouldComponentUpdate(nextProps, nextState) {
        if(JSON.stringify(this.state) !== JSON.stringify(nextState))
            return true;
        if(JSON.stringify(this.props) === JSON.stringify(nextProps))
            return false;
        else
            return true;
    }

    compareListItems(a, b) {
        return a.position > b.position ? 1 : -1;
    }
}
