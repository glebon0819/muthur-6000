import React from 'react';
import { ReactSVG } from 'react-svg';

import Button from '../Button';
import ButtonDanger from '../ButtonDanger';
import ButtonSuccess from '../ButtonSuccess';
import List from './List';
import ListName from './ListName';
import Popup from '../Popup';
import Window from '../Window';

import Checkmark from '../../checkmark.svg';
import Hourglass from '../../hourglass.svg';
import Trashcan from '../../trashcan.svg';

export default class Lists extends React.Component {

    // add blank new list item to the database
    addListItem() {
        this.toggleLoading();
        fetch('/api/lists/' + this.state.selectedListID + '/addItem', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: ''
        }).then((res) => {
            this.toggleLoading();
        });
    }

    // changes the list contents that display on the right side of the screen
    changeList(list) {

        // sets the state to include the new content
        this.setState({
            list: list,
            selectedListID: list.id
        });
    }

    componentDidMount() {

        // retrieve all lists, including checklists, from DB
        fetch('/api/lists/all')
        .then(res => res.json())
        .then(
            (result) => {
                var regularLists = [], checklists = [];
                result.forEach((list) => {
                    if(list.checklist)
                        checklists.push(list);
                    else
                        regularLists.push(list);
                });

                this.setState({
                    checklists: checklists,
                    list: result[0],
                    regularLists: regularLists,
                    selectedListID: result[0].id
                });
            },
            (error) => {
                console.error(error);
            }
        );
    }

    componentDidUpdate(prevProps) {
        if(prevProps.appHeight !== this.props.appHeight) {
            this.setState({
                appHeight: this.props.appHeight
            });
        }
    }

    // construct the Lists object with default state and bind the object to its methods
    constructor(props) {
        super(props);
        this.state = {
            appHeight: this.props.appHeight,
            checklists: [],
            content: null,
            itemNames: {},
            list: null,
            regularLists: [],
            loading: false,
            name: 'Lists',
            selectedListID: null,
            showChecklistCreator: false,
            showListCreator: false,
            updatedItemNames: {}
        };
        this.addListItem = this.addListItem.bind(this);
        this.changeList = this.changeList.bind(this);
        this.handleChecklistItemCheck = this.handleChecklistItemCheck.bind(this);
        this.onPopupClose = this.onPopupClose.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
        this.toggleLoading = this.toggleLoading.bind(this);
        this.updateListItemName = this.updateListItemName.bind(this);
    }

    handleChecklistItemCheck(itemID, status) {
        this.setState({
            loading: true
        }, () => {
            fetch('/api/lists/' + itemID + '/check', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ status: status })
            }).then((res) => {
                console.log(res);
                this.setState({
                    loading: false
                });
            });
        });
    }

    // when popup closes, add new list to database
    onPopupClose(result, checklist) {
        if(typeof checklist == 'undefined')
            var checklist = false;

        // hide the popup
        this.setState({
            showChecklistCreator: false,
            showListCreator: false
        });

        // if popup returned a list name, add it to the database
        if(result !== false && result.length !== 0) {
            this.toggleLoading();
            var listName = { checklist: checklist, listName: result };
            fetch('/api/lists/addList', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(listName)
            }).then((res) => {
                this.toggleLoading();
            });
        }
    }

    // render the "Lists" application
    render() {
        var checklistNames = '',
            content = '',
            regularListsNames = '';
        if(this.state.list !== null)
            content = <List
                appHeight={ this.props.appHeight }
                handleChecklistItemCheck={ this.handleChecklistItemCheck }
                list={ this.state.list }
                listID={ this.state.selectedListID }
                loading={ this.state.loading }
                toggleLoading={ this.toggleLoading }
                updateListItemName={ this.updateListItemName } />;

        // generate components from the checklist names if there are any yet
        if(this.state.checklists.length > 0) {
            checklistNames = this.state.checklists.map((list, idx) => {
                return (
                    <ListName
                        id={ list.id }
                        key={ 'listName-' + idx }
                        list={ list }
                        selected={ (this.state.selectedListID == list.id) ? true : false }
                        changeList={ this.changeList } />
                );
            });
        }

        // generate components from the regular list names if there are any yet
        if(this.state.regularLists.length > 0) {
            regularListsNames = this.state.regularLists.map((list, idx) => {
                return (
                    <ListName
                        id={ list.id }
                        key={ 'listName-' + idx }
                        list={ list }
                        selected={ (this.state.selectedListID == list.id) ? true : false }
                        changeList={ this.changeList } />
                );
            });
        }

        var windowHeight = this.state.appHeight / 2 - 16 - 16 - 8;
        return (
            <div className='row'>
                <div className='col-sm-4' style={{ paddingRight: 0 }}>
                    <Window style={{ height: windowHeight, maxHeight: windowHeight }} title='Your Lists'>
                        <div style={{ height: windowHeight - 38 - 16, maxHeight: windowHeight - 38 - 16 - 16, overflowY: 'scroll' }}>
                            { regularListsNames }
                        </div>
                        <ButtonSuccess
                            onClick={ () => {
                                this.setState({
                                    showListCreator: true
                                });
                            }}
                            text='Add New List' />
                        <Popup
                            callback={ this.onPopupClose }
                            showPopup={ this.state.showListCreator } />
                    </Window>
                    <Window style={{ height: windowHeight, maxHeight: windowHeight }} title='Your Checklists'>
                        <div style={{ height: windowHeight - 38 - 16, maxHeight: windowHeight - 38 - 16 - 16, overflowY: 'scroll' }}>
                            { checklistNames }
                        </div>
                        <ButtonSuccess
                            onClick={ () => {
                                this.setState({
                                    showChecklistCreator: true
                                });
                            }}
                            text='Add New Checklist' />
                        <Popup
                            callback={ (input) => this.onPopupClose(input, true) }
                            showPopup={ this.state.showChecklistCreator } />
                    </Window>
                </div>

                { /* render window on the right side of the screen */ }
                <div className='col-sm-8' style={{ paddingLeft: 0 }}>
                    <div className='window' style={{ height: this.props.appHeight - 60, maxHeight: this.props.appHeight - 60, overflow: scroll }}>
                        <div className='window-banner'>{ this.state.list !== null ? this.state.list.name : '' }</div>
                        <div className='window-banner-content'>

                            { /* render list items from current list or an error message */ }
                            { content }

                            { /* render buttons to add new items or save changes */ }
                            <div className='row'>
                                <div className='col-sm-1' style={{ alignItems: 'center', display: 'flex', justifyContent: 'center', paddingRight: 0 }}>
                                    <ReactSVG
                                        src={ this.state.loading ? Hourglass : Checkmark }
                                        beforeInjection={(svg) => {
                                            svg.classList.add('svg-poop')
                                            svg.setAttribute('style', 'fill: #FFF')
                                        }}
                                        fallback={() => <span>loading</span>} />
                                </div>
                                <div className='col-sm-5' style={{ paddingLeft: 0, paddingRight: 0 }}>
                                    <ButtonSuccess onClick={ this.addListItem } text='Add List Item' />
                                </div>
                                <div className='col-sm-5' style={{ paddingLeft: 0, paddingRight: 0 }}>
                                    <Button onClick={ this.saveChanges } text='Save Changes' />
                                </div>
                                <div className='col-sm-1' style={{ paddingLeft: 0 }}>
                                    <ButtonDanger icon={ Trashcan } onClick={ this.toggleLoading } />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    // package up data regarding changes and send it to the server
    saveChanges() {
        this.toggleLoading();
        var itemNames = {
            updatedItemNames: this.state.updatedItemNames,
            listID: this.state.selectedListID
        };
        fetch('/api/lists/' + this.state.selectedListID + '/save', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(itemNames)
        }).then((res) => {
            this.toggleLoading();
        });
    }

    // change icon to either an hourglass (if loading) or a checkmark (if complete)
    toggleLoading() {
        this.setState({
            loading: !this.state.loading
        });
    }
    updateListItemName(newItemName) {
        this.setState({
            updatedItemNames: Object.assign(this.state.updatedItemNames, newItemName)
        });
    }
}
