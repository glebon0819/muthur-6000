import React from 'react';

export default class ListName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false
        };
    }
    render() {
        var classes = this.props.selected ? 'list-name list-name-selected' : 'list-name';
        return (
            <div className={ classes } onClick={ () => /*this.props.changeSelectedListID(this.props.list)*/this.props.changeList(this.props.list) }>
                <span>{ this.props.list.name }</span>
            </div>
        );
    }
}
