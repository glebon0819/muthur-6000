import React from 'react';
import ReactDOM from 'react-dom';
import { ReactHeight } from 'react-height';

//import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-bootstrap-toggle/dist/bootstrap2-toggle.css'
import './App.css';
import './Tasks.css';

import Schedule from './components/Schedule';
import Tasks from './components/Tasks';
import Lists from './components/lists/Lists';

class App extends React.Component {

    // define the application's state
    constructor(props) {
        super(props);
        this.viewportHeight = document.documentElement.clientHeight;
        this.viewportWidth = document.documentElement.clientWidth;
        this.state = {
            appHeight: 0,
            selectedApplication: 'Schedule'
        };
        this.changeSelectedApplication = this.changeSelectedApplication.bind(this);
    }
    componentDidUpdate() {
        //console.log('App Height: ' + this.state.appHeight);
    }

    // define function for changing the selectedApplication
    changeSelectedApplication(newApplication) {
        this.setState({
            selectedApplication: newApplication
        });
    }

    // render the shell components & create the framework for the application
    render() {

        // define the applications to chose from
        var applications = [ 'Schedule', 'Tasks', 'Lists' ];
        var applicationList = applications.map((application, idx) => {
            return (
                <div className={ 'application' + (application == this.state.selectedApplication ? ' selected' : '') } onClick={ () => { this.changeSelectedApplication(application) } }>
                    <span className='application-name'>{ application }</span>
                </div>
            );
        });

        // define which application component to render based on the selected application
        var content;
        switch(this.state.selectedApplication) {
            case 'Tasks':
                content = <Tasks appHeight={ this.state.appHeight } />;
                break;
            case 'Lists':
                content = <Lists appHeight={ this.state.appHeight } />;
                break;
            default:
                content = <Schedule appHeight={ this.state.appHeight } />;
        }

        // define the UI components to be rendered
        return (
            <div className='app' style={{height: this.viewportHeight}}>
                <div id='banner'>MU/TH/UR 6000</div>
                <div id='viewport'>
                    <div id='sidebar' style={{height: this.viewportHeight - 16 - 4, width: this.viewportWidth / 4}}>
                        <h6 style={{padding: 8}}>Applications</h6>
                        { applicationList }
                    </div>
                    <div id='content' style={{height: this.viewportHeight - 16 - 4, width: (this.viewportWidth / 4) * 3}}>
                        <ReactHeight onHeightReady={value => this.setState({appHeight: value})}>
                            <div id='application-window' style={{ height: this.viewportHeight - 16 - 4 - 32 - 4}}>
                                <div id='application-window-banner' style={{ margin: 0 }}>App Name</div>
                                <div id='application-window-content'>
                                    { content }
                                </div>
                            </div>
                        </ReactHeight>
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
